# How to use

Requires [discord application](https://discordapp.com/developers/applications)

1. Create .env based off of .env.example with token from your discord bot and channel name.

If using yarn:

2. yarn install
3. yarn start

If using npm:

2. npm install
3. npm start
