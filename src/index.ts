import axios from "axios";
import Discord, { TextChannel } from "discord.js";
import dotenv from "dotenv";
import state from "./util/state";
import WorldState from "warframe-worldstate-parser";

dotenv.config();

const client = new Discord.Client();

const interval =
  (process.env.INTERVAL && parseInt(process.env.INTERVAL, 10) * 60000) || 60000;

const channelName = process.env.CHANNEL || "warframe-chat";

const rewardTypes = ["catalyst", "reactor"];

client.on("ready", async () => {
  console.log("Connected to discord.");
  let prevState = await state.load();
  const runAlert = async () => {
    try {
      const response = await axios.get(
        "http://content.warframe.com/dynamic/worldState.php"
      );
      const worldState = new WorldState(JSON.stringify(response.data));
      state.save(worldState);
      const orokinInvasions = worldState.invasions.filter(invasion =>
        invasion.rewardTypes.some(rt => rewardTypes.includes(rt.toLowerCase()))
      );
      const prevOrokinInvasions = prevState.invasions.filter(invasion =>
        invasion.rewardTypes.some(rt => rewardTypes.includes(rt.toLowerCase()))
      );
      orokinInvasions.forEach(invasion => {
        if (!prevOrokinInvasions.some(x => x.id === invasion.id)) {
          console.log(`Results found.`);
          const channels = client.channels.filter(x => x.type === "text");
          if (channels) {
            channels.forEach(channel => {
              const textChannel = channel as TextChannel;
              if (textChannel.name.includes(channelName)) {
                textChannel.send(
                  `__**INVASION!**__ ${invasion.desc} *at* ${
                    invasion.node
                  }.\n**Rewards**: ${invasion.rewardTypes.join(", ")}`
                );
              }
            });
          }
        }
      });
      prevState = JSON.parse(JSON.stringify(worldState));
    } catch (error) {
      console.error(error.message);
    }
  };
  setInterval(() => runAlert(), interval);
  runAlert();
});

// if disconnected, retry every minute.
client.on("disconnect", () => {
  console.log("Disconnected... retrying in 60 seconds.");
  const interval = setInterval(() => {
    client.login(process.env.TOKEN).then(_ => {
      clearInterval(interval);
    });
  }, 60000);
});

client.login(process.env.TOKEN);
