import fs from "fs";
import { promisify } from "util";
import WorldState from "warframe-worldstate-parser";
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);

export const save = async (worldState: WorldState) => {
  try {
    const json = JSON.stringify(worldState);
    await writeFile("data/worldstate.json", json, { encoding: "utf8" });
  } catch (error) {
    console.error(error.message);
  }
};

export const load = async (): Promise<WorldState> => {
  try {
    const json = await readFile("data/worldstate.json", { encoding: "utf8" });
    return JSON.parse(json);
  } catch (error) {
    console.error(error.message);
    return { invasions: [] };
  }
};

export default { save, load };
